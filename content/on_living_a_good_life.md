+++
title = "On Life and death"
description = ""
date = "2017-12-30"
menu = "main"
+++

Stoic philosophy declares that certain things are under our control, and some not, see *The Enchiridion*
by Epictetus (par. 1). According to Epictetus, the only thing under our control is how we decide
to perceive certain events. The death of a child is a catastrophe for the parents of the child, but
for a stranger, this is simply an event that has no bearing on him. Thus, the death of one's own
child should also be perceived as such (par. 26, *The Enchiridion*). If, on the contrary, one wishes
for his family to live forever, he is a fool, for he wants something that is not under his control
(par. 14, *The Enchiridion*). Similar ideas can be found in the *Moral Letters to Lucilius* by Seneca
the Younger (LXIII. *On Grief for Lost Friends*). How we should perceive the death of loved ones
is also how we should perceive our own mortality. *Memento mori*, remember that you have to die. This
idea is also very important in Christianity (which borrowed a lot of ideas from stoicism, see
the works of Philo of Alexandria, and Origen Adamantius). Marcus Aurelius, the last of the Five
Good Emperors, also wrote on the subject in his *Meditations*, Book IV, par. XIV *Not as though thou hadst
thousands of years to live. Deaths hangs over thee: whilst yet thou livest, whilst thou mayest, be good*.

So if death itself is inevitable, and only how we perceive death is under our control, what about life?
For Marcus Aurelius, a good life is one that is in accordance with nature, (*Meditations*
Book I, par. VI, *Meditations* Book IV, par. XLIII), which also means being good.
Going back to *Meditations*, Book IV, par. XIV, Marcus Aurelius exhorts the reader (himself), to be
good, again in Book VII, par. XII, and again in Book XI, par. IV.

Living in accordance to nature means that one should live the life for which he was put here on
this earth (*Meditations* Book V, par. I). The bees do not ask themselves if they can stay long in bed
and hit the snooze button, they just get up and start their day. You have duties to perform and when
duty calls, you go and perform them, even if you have to abandon your loved ones
(*The Enchiridion*, par. 7). Living according to nature is also using your reason
(*Meditations* Book VII, par. VIII). Using your reason means that you should pursue things that are
good for you, and despise things that are bad for you. For example, eat food that is nutritious and
healthy, but with temperance. Do not torture your body and do not be dirty on purpose (*Moral Letters
to Lucilius*, V). Unless you do it on purpose, as a ways to strengthen you; fast some days of the
week, sleep on the floor and take cold baths, see Letter XVIII.

Thus having lived according to nature, having performed his duties, and having been a good man,
you can leave this earth happily, *Meditations* Book V, par. XXX
*Let death surprise rue when it will, and where it will, I may be a happy man,
nevertheless. For he is a happy man, who in his lifetime dealeth unto himself a happy lot and portion.
A happy lot and portion is, good inclinations of the soul, good desires, good actions.*

### References:

[The Enchiridion](http://classics.mit.edu/Epictetus/epicench.html)

[Meditations](http://www.gutenberg.org/files/2680/2680-h/2680-h.htm)

[Moral letters to Lucilius](https://en.wikisource.org/wiki/Moral_letters_to_Lucilius)
