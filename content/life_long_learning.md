+++
title = "On lifelong learning"
description = ""
date = "2018-04-08"
menu = "main"
+++

Evidence shows that the more skills you have, the better are your job prospects, as well as your health
and social life. You can read [this report](https://read.oecd-ilibrary.org/education/skills-matter_9789264258051-en)
by the OECD for more details. This means that you should keep accumulating skills and knowledge all throughout
your life. Also, learning new things, and reading help keep Alzheimer's disease at bay. There's so much
evidence that this is true that I won't post any sources here. As usual, research everything you read
here for yourself. But let's go back to accumulating skills throughout your life. In the post
where I discuss physical activity, I quoted Seneca. Let me quote him again:

*each day acquire something that will fortify you against poverty, against death, indeed against other misfortunes as well.*

This quote can be interpreted as an exhortation to keep learning new skills. And today, with the
Internet there is absolutely no excuse to not keep learning new skills that could prove valuable for
the labour market, or to simply improve your day to day life, such as cooking. You can watch youtube
channels, read blogs, Wikipedia, but perhaps the most useful thing you can do online is taking MOOCs.
MOOCs are online classes that you can take on sites like [Coursera](https://www.coursera.org/).
A lot of different topics are covered, and some of the courses grant degrees and professional certificates
that you can add to your Linkedin profile (if you use Linkedin). Or you can simply learn for the pleasure
of learning and acquiring new skills. Coursera is not the only such site, you can find a lot of
them using any seach engine.

Learning new hard skills, such as computer programming is great, but there are another set of skills
that you can learn, called soft skills. These skills cover things like learning how to hold an
adult conversation with strangers you meet at a party for instance. Classics such as *How to win Friends
and influence people* by Dale Carnegie can help you learn more about getting better at soft skills.

Whatever the skill you choose to learn or improve, and whatever the method (MOOC, book, etc), remember
what Seneca told Lucilius, and each day, improve your self. Do not let a single day go to waste.
