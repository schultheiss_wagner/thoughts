+++
title = "On nutrition"
description = ""
date = "2018-01-01"
menu = "main"
+++

You probably have heard that you are what you eat. A similar sentence is attributed to Hippocrates
(but nowhere to be found in his writings):

*Let food be thy medicine and let medicine be thy food*

Both these quotes might not be entirely true and exact, but what you eat definitely has an impact
on your health. These impacts can be very difficult to quantify. In fact, for a lot of different foods
you can find evidence that they are good for you, but also bad for you! So where does that leave us?

I am not a nutritionist, nor a medical doctor, so do not believe anything you are going to read here.
If you are still curious, verify every claim I make yourself. I think that through common sense and
by reading people that know about these topics, you can start to have a pretty clear idea of what
you should eat, and how much of it.

First of all, in terms of quantities, if you are an average man you probably should eat around 2000
calories a day. This number is just an average though; you may require more or less. You can have
some idea of the amount you should eat by computing your BMR, or basal metabolic rate. You can find
tools online that compute it for you. But a good rule of thumb is the following: if you are putting
on weight, you are eating too much. This is a simple rule of thumb that works in 99% of the cases.
This is common sense at play; if you do not eat excess calories, your body simply cannot store them as fat.

Let me repeat this:

It is not possible to gain weight over an extended period of time if you are not eating excess calories.

If, however, you are tracking your calories and eating less than your BMR, and you are gaining weight,
then seek medical help because you might be suffering from a dangerous chronic disease, such as
hypothyroidism. But the more likely explanation is that you made a mistake computing your BMR, or
you are not tracking your calories correctly.

But why keep track of the quantity of what you eat? Well eating in excess is a cause (most probably
the most important cause) of obesity. Obesity is linked to, or the cause of, type 2 diabetes,
cardiovascular diseases, sleep apnea, cancer... and the cause of all that is eating in excess.
There is some evidence that genetics might play a role, however, this would simply be a predisposition,
not an unavoidable fate. You can definitely avoid being obese by managing the quantity of food you eat.

If you have trouble eating less, this might be because you are not eating the right foods. Also, it
helps to think of this quote by Marcus Aurelius:

*How marvellous useful it is for a man to represent unto himself meats, and all such things that are
for the mouth, under a right apprehension and imagination! as for example: This is the carcass of a
fish; this of a bird; and this of a hog. And again more generally; This phalernum, this excellent
highly commended wine, is but the bare juice of an ordinary grape. This purple robe, but sheep's
hairs, dyed with the blood of a shellfish. So for coitus, it is but the attrition of an ordinary
base entrail, and the excretion of a little vile snivel, with a certain kind of convulsion:
according to Hippocrates his opinion.* (Book VI, par. 11)

Thinking of food as the dead bodies of animals or as simple juices in the case of drinks can help you
control yourself. Also think of temperance, an important virtue, or gluttony, one of the deadly
sins in Christianity. Don't you want to be virtuous?

Another way to eat less, and train your self-control, is to fast. Read about intermittent fasting,
which consists in not eating from say, dinner to lunch (so just skipping breakfast), or not eat
during 24 hours 2 times a week. There is some evidence that intermittent fasting might have positive
health impacts in addition to reducing your caloric intake.

Now, what should you eat? Forget about temporary diets. If you are putting on weight and eating
foods that are unhealthy, a fad diet will not help you lose weight. And eating less of unhealthy foods
will be very difficult, because you will feel hungry and frustrated all the time. You will be fighting
your willpower, and your willpower is a terrible opponent. What you need is a lifestyle change. You have
to switch to different kinds of foods.

I believe that the best way is not to eat less, but eat more of (better yet, only) good, filling foods.
You will not feel hungry all the time, will not feel frustrated and you will not have to fight your willpower.

First of all, eat vegetables. Lots, and lots of vegetables. Vegetables are low in calories, rich
in vitamins and minerals. There are literally hundreds, if not thousands of different kinds of
vegetables, and you can cook them is so many different ways. You are bound to find something that suits
your tastes. Now, I am talking about vegetables, which includes more that simply green salad. For
some reason, people always picture the boring green salads when I talk about vegetables. Oh but you
do not like vegetables? I do not believe you. You can not dislike *all** vegetables. You might not
like cabbage; eat spinach then. And if you do not really like any vegetables, eat them anyways. Food
does not always need to be about pleasure.

*Thou shouldst eat to live; not live to eat.*, Socrates.

Think of the long term impact of eating unhealthy (albeit tasty because coated in sugar and transfats)
foods. Is it really worth all the health issues? A very good way to eat vegetables, is to prepare soups.
Soups are very filling, low calorie, easy and fast to make.

Also, you should eat fresh fruit. As for protein, favor lean protein, but just keep in mind that you
do not need to eat animal protein for each meal. There is evidence that eating animal protein in
excess is not good for you, but establishing a causal link is very difficult. But, what does *in excess*
mean? Well, certainly at each meal. Recommendations are around 50 grams of protein a day (for men
and women), and this can come from vegetarian sources such as legumes. You can also go completely
vegetarian and still get more than enough protein.

To finish, what should you simply stop eating? Stop eating everything that is industrially processed.
Forget about soft drinks, sweet or salty snacks (eat nuts instead, but watch your calories!), fast
food... you know exactly what to avoid, actually. Of course, this does not mean that you should never
again have a piece of cake; but this should be for important and particular events, such as a birthday,
or Christmas. Same for a soft drink; if your primary drink is not water but soft drinks, you will
have very grave health issues. Soft drinks should be reserved for when you go to the pub with your
friends, and if you drink alcohol, drink a beer, or a glass of wine, it's healthier than a soft drink.
Eating these foods is contrary to nature; they are not healthy, and they do not provide nutrition.
Basically, just stop eating simple sugars.

I think that temperance, a very important virtue that you can find in a lot of civilizations and
religions is very difficult to practice, but is one where you should concentrate your efforts, and
very relevant for eating better, and eating the right amount.
