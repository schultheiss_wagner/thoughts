+++
title = "On physical activity"
description = ""
date = "2017-12-31"
menu = "main"
+++

If acting according to nature is pursuing things that are good for us and avoiding things that are
bad for us, doing some kind of physical activity is certainly part of the things that are good for
us (*Moral Letters to Lucilius, Letter V*). In Letter II, Seneca advises Lucilius to

*each day acquire something that will fortify you against poverty, against death, indeed against
other misfortunes as well*.

It has been proved, and known since Hippocrates, that people that are physically active
have better and longer lives (no need for citations here, I believe). Socrates too, shared this opinion:

*No man has the right to be an amateur in the matter of physical training. It is a shame for a man to
grow old without seeing the beauty and strength of which is body is capable* (in *Memorabilia*).

For Socrates, it was a matter of life and death:

*And yet they are not a few who, owing to a bad habit of body, either perish outright in the perils
of war, or are ignobly saved. Many are they who for the self-same cause are taken prisoners, and being
taken must, if it so betide, endure the pains of slavery for the rest of their days* (in *Memorabilia*).

![You are the sculptor and the marble](/marble_and_artist.jpg)


"But we don't live in ancient Greece anymore! There is no war, and there is the police! Plus I am
healthy."

Do you live in a country or city where there is never any kind of mugging, violence or any terrorist
threat? Do you always carry a police officer on you? If to any of these questions you answer "no",
don't you think it would be better if you could have the physical capability to defend yourself, or,
at the very least, be able to run long and fast enough to escape any danger? Also, do you think
you will stay forever young? If you have a desk job, poor eating habits, smoke, drink too much
alcohol and spend your nights watching American TV shows, how long do you think you will stay healthy?

"But where to start?"

I believe that the most important part of physical activity is to pick something that you enjoy. If
you like running, go for runs. If you enjoy swimming, start swimming. However, if you want to stay
(or become) healthy and also prepare your body for physical confrontation, I suggest you pick up some
form of martial art. Boxing, wrestling, oriental martial arts, anything is good. Buy a jumping rope
and start skipping rope to increase your endurance. Do calisthenics to fortify your body. A good source
on calisthenics is *Convict Conditioning* by Paul Wade. But do this diligently:
read par. 29 of *The Enchiridion*. By doing martial arts, not only will you increase your health by
being active, but you will also acquire skills that could prove useful one day; even if you never
want to be in a situation where you will need them.

*The supreme art of war is to subdue your ennemy without fighting.* (*The Art of War*, Sun Tzu)

By being fit and having a body that is prepared for the worst case scenario, you will emit an aura
of confidence and you will be more alert, and avoid troublesome situations better.

However, physical activity is only one part of improving your body and mind; another part is nutrition.
As Hippocrates noted in Book I of *On Dietetics*:

*Food and exercise, while possessing opposite properties, nevertheless mutually contribute to
maintaining health.*

Nutrition is another topic where I'll share some thoughts.

Do you think this is a waste of your time? Why, are you so busy that you cannot spare 2 to 4 hours
a week to increase your physical and mental health, and acquire skills that might one day save your,
or your loved ones, life? Is wasting your time on mindless activities, just to have your mind be
entertained by television, video games, social media, etc during all your waking hours that much
more important?

### References:

Picture by Andrey Cherlat on [500px](https://500px.com/photo/186633789/athletic-man-cuts-his-body-of-marble-stone-by-andrey-cherlat)


[The Enchiridion](http://classics.mit.edu/Epictetus/epicench.html)

[Meditations](http://www.gutenberg.org/files/2680/2680-h/2680-h.htm)

[Moral letters to Lucilius](https://en.wikisource.org/wiki/Moral_letters_to_Lucilius)

[The Memorabilia](http://www.gutenberg.org/files/1177/1177-h/1177-h.htm)
